
public class Punto {

    double x;
    double y;

    public Punto(double _x, double _y) {
        this.x = _x;
        this.y = _y;
    }

    public double getX() {
        return this.x;
    }

    public double getY() {
        return this.y;
    }

    public String ToString() {

        return ("X=" + this.x + "       Y=" + this.y);
    }
}
