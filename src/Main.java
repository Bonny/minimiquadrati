
import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.UIManager;

public class Main extends javax.swing.JFrame {

  private double x = 0, y = 0, pi = 0, q = 0, covarianza_xy = 0, varianza_x = 0, beta = 0, alpha = 0;
  private Punto punti[];
  private java.util.List<Punto> lista = new java.util.ArrayList<Punto>();
  private Piano piano;
  private boolean stato = false;
  public static final String INT = "([\\-]?[0-9]+)",
          FLOAT = "([\\-]?[0-9]+\\.([0-9]+))",
          INR = "([\\-]?[0-9]+[\\/]([1-9]+[0-9]*+))";
  public static final String NUMERIC = INT + "|" + FLOAT + "|" + INR;

  public Main() {
    initComponents();

    initValori();
    Dimension dim = p.getSize();
    piano = new Piano(dim);
    p.add(piano);

    piano.repaint();
    Toolkit mioToolkit = Toolkit.getDefaultToolkit();
    Dimension dimensioniSchermo = mioToolkit.getScreenSize();
    this.setBounds((int) (dimensioniSchermo.getWidth() / 2) - this.getWidth() / 2, (int) (dimensioniSchermo.getHeight() / 2) - this.getHeight() / 2, this.getWidth(), this.getHeight());
  }

  @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        chooser = new javax.swing.JFileChooser();
        p = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        list = new javax.swing.JList();
        txtdy = new javax.swing.JTextField();
        txtdx = new javax.swing.JTextField();
        btnadd = new javax.swing.JButton();
        btnrimuovi = new javax.swing.JButton();
        btnall = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        btng = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        ascissa = new javax.swing.JLabel();
        ordinata = new javax.swing.JLabel();
        Pxy = new javax.swing.JLabel();
        Qxy = new javax.swing.JLabel();
        var = new javax.swing.JLabel();
        covar = new javax.swing.JLabel();
        coef = new javax.swing.JLabel();
        iters = new javax.swing.JLabel();
        retta = new javax.swing.JLabel();
        save = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Minimi Quadrati");
        setResizable(false);

        p.setBackground(new java.awt.Color(204, 255, 204));
        p.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));

        javax.swing.GroupLayout pLayout = new javax.swing.GroupLayout(p);
        p.setLayout(pLayout);
        pLayout.setHorizontalGroup(
            pLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 612, Short.MAX_VALUE)
        );
        pLayout.setVerticalGroup(
            pLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 575, Short.MAX_VALUE)
        );

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Pannello"));

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel1.setText("Coordinate");

        jScrollPane1.setViewportView(list);

        btnadd.setText("Aggiungi");
        btnadd.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnaddMouseClicked(evt);
            }
        });

        btnrimuovi.setText("Rimuovi");
        btnrimuovi.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnrimuoviMouseClicked(evt);
            }
        });

        btnall.setText("Nuovo");
        btnall.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnallMouseClicked(evt);
            }
        });

        jLabel2.setText("X");

        jLabel3.setText("Y");

        btng.setText("Grafico");
        btng.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btngMouseClicked(evt);
            }
        });

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Valori"));

        ascissa.setText("jLabel4");

        ordinata.setText("jLabel4");

        Pxy.setText("jLabel4");

        Qxy.setText("jLabel4");

        var.setText("jLabel4");

        covar.setText("jLabel4");

        coef.setText("jLabel4");

        iters.setText("jLabel4");

        retta.setText("jLabel4");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(coef, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(covar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(var, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(Qxy, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(ascissa, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(ordinata, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(Pxy, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(iters, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(retta, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(349, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(ascissa)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(ordinata)
                .addGap(11, 11, 11)
                .addComponent(Pxy)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(Qxy)
                .addGap(13, 13, 13)
                .addComponent(var)
                .addGap(9, 9, 9)
                .addComponent(covar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(coef)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(iters)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(retta)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        save.setText("Salva");
        save.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                saveMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(jLabel1))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 155, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(10, 10, 10)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(jLabel3))
                        .addGap(4, 4, 4)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(btnall, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED))
                            .addComponent(btnrimuovi, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtdx, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtdy, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnadd, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(save, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(btng, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED))))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(11, 11, 11)
                .addComponent(jLabel1)
                .addGap(11, 11, 11)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addComponent(jLabel2)
                        .addGap(24, 24, 24)
                        .addComponent(jLabel3))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(txtdx, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(txtdy, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnadd)
                        .addGap(18, 18, 18)
                        .addComponent(btnrimuovi)
                        .addGap(18, 18, 18)
                        .addComponent(btnall)
                        .addGap(18, 18, 18)
                        .addComponent(btng)
                        .addGap(18, 18, 18)
                        .addComponent(save))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 263, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(p, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(p, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
//bottone aggiungi
    private void btnaddMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnaddMouseClicked

      String dx = txtdx.getText();
      String dy = txtdy.getText();

      if (dx.equals("") || dy.equals("")) {
        JOptionPane.showMessageDialog(null, "Deviriempire tutti i campi", "Valori non validi", JOptionPane.WARNING_MESSAGE);
      } else {
        if (!dx.matches(NUMERIC) || !dy.matches(NUMERIC)) {
          JOptionPane.showMessageDialog(null, "Devi inserire solo valori numerici", "Valori non validi", JOptionPane.WARNING_MESSAGE);
        } else {
          Punto punto = new Punto(tronca(getValue(dx)), tronca(getValue(dy)));
          lista.add(punto);
          updateList();
        }
      }
    }//GEN-LAST:event_btnaddMouseClicked
//bottone rimuovi
    private void btnrimuoviMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnrimuoviMouseClicked
      int index = list.getSelectedIndex();
      if (index >= 0) {
        lista.remove(index);
        updateList();
      } else {
        JOptionPane.showMessageDialog(null, "Se non selezioni niente non rimuovo niente!", "Messaggio", JOptionPane.INFORMATION_MESSAGE);
      }
    }//GEN-LAST:event_btnrimuoviMouseClicked
//bottone nuovo
    private void btnallMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnallMouseClicked
      if (!lista.isEmpty()) {
        lista.clear();
        updateList();
        initValori();
        piano.repaint();

      }
    }//GEN-LAST:event_btnallMouseClicked
//bottone grafico, calcola e disegna su 'Piano' la retta di regressione
    private void btngMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btngMouseClicked

      if (!lista.isEmpty()) {

        x = y = pi = q = covarianza_xy = varianza_x = beta = alpha = 0;
//i numeri x,y,q,p rappresentano le medie aritmetiche delle ascisse,delle ordinate,
//dei quadrati delle ascisse e dei prodotti ascissa per ordinata.
        for (int i = 0; i < lista.size(); i++) {
          x += lista.get(i).getX() / lista.size();
          y += lista.get(i).getY() / lista.size();
          q += (lista.get(i).getX() * lista.get(i).getX()) / lista.size();
          pi += (lista.get(i).getX() * lista.get(i).getY()) / lista.size();
        }
        varianza_x = q - (x * x);
        covarianza_xy = pi - (x * y);
        //coeff angolare
        beta = covarianza_xy / varianza_x;
        //intersezione asse y
        alpha = y - (beta * x);
        // percio la retta di regressione è y = beta*x - alpha
        ascissa.setText("Media Ascissa:  " + tronca(x));
        ordinata.setText("Media ordinata:  " + tronca(y));
        Pxy.setText("Media prodotto x*y:  " + tronca(pi));
        Qxy.setText("Media x*x:  " + tronca(q));
        coef.setText("Coefficente Angolare:  " + tronca(beta));
        covar.setText("Covarianza di xy:  " + tronca(covarianza_xy));
        var.setText("Varianza di x: " + tronca(varianza_x));
        iters.setText("Intersezione asse y:  " + tronca(alpha));
        retta.setText("Retta di regressione:   Y = " + tronca(beta) + "*X" + " " + tronca(alpha));
        punti = lista.toArray(new Punto[lista.size()]);
        piano.set(beta, alpha, punti);
        piano.repaint();
        stato = true;
      }
    }//GEN-LAST:event_btngMouseClicked
//bottone salva, salva in un file di testo tutte le informazioni correnti
    private void saveMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_saveMouseClicked
      if (stato) {
        System.out.println("ok");
        int ret = chooser.showDialog(null, "Save file");
        if (ret == JFileChooser.APPROVE_OPTION) {
          try {
            PrintWriter foo = new PrintWriter(new FileWriter(chooser.getSelectedFile().toString() + ".txt"));

            foo.println("Coordinate:");
            foo.println("**************************************************");
            for (int i = 0; i < punti.length; i++) {
              foo.println(punti[i].ToString());
            }
            foo.println("**************************************************");
            foo.println("Valori Calcolati:\n");
            foo.println("Media Ascissa:  " + tronca(x));
            foo.println("\nMedia ordinata:  " + tronca(y));
            foo.println("\nMedia prodotto x*y:  " + tronca(pi));
            foo.println("\nMedia x*x:  " + tronca(q));
            foo.println("\nCoefficente Angolare:  " + tronca(beta));
            foo.println("\nCovarianza di xy:  " + tronca(covarianza_xy));
            foo.println("\nVarianza di x: " + tronca(varianza_x));
            foo.println("\nIntersezione asse y:  " + tronca(alpha));
            foo.println("\nRetta di regressione:   Y = " + tronca(beta) + "*X" + " " + tronca(alpha));
            foo.close();
          } catch (IOException ex) {
            JOptionPane.showMessageDialog(null, "Errore", "Errore", JOptionPane.WARNING_MESSAGE);
          }
        }

      } else {
        JOptionPane.showMessageDialog(null, "Nessun Valore da salvare.", "Messaggio", JOptionPane.INFORMATION_MESSAGE);
      }
}//GEN-LAST:event_saveMouseClicked

  private double getValue(String s) {
    double r = 0;
    if (s.matches(INR)) {
      String[] t = s.split("/");
      r = Double.parseDouble(t[0]) / Double.parseDouble(t[1]);
    } else {
      r = Double.parseDouble(s);
    }
    return r;
  }
//metodo per aggiornare il Jlist dopo un inserimento

  private void updateList() {
    Punto pp[] = lista.toArray(new Punto[lista.size()]);
    String t[] = new String[lista.size()];
    for (int i = 0; i < t.length; i++) {
      t[i] = pp[i].ToString();
    }
    list.setListData(t);
    txtdx.setText("");
    txtdy.setText("");
    txtdx.requestFocus();
  }
//inizializza il pannello Valori

  private void initValori() {
    ascissa.setText("Media Ascisse: ");
    ordinata.setText("Media ordinate: ");
    Pxy.setText("Media prodotto x*y: ");
    Qxy.setText("Media x*x: ");
    coef.setText("Coefficente Angolare: ");
    covar.setText("Covarianza di xy: ");
    var.setText("Varianza di x: ");
    iters.setText("Intersezione asse y: ");
    retta.setText("Retta di regressione: ");
    txtdx.requestFocus();
    stato = false;
  }

  private double tronca(double val) {
    BigDecimal bg = new BigDecimal(val);
    bg = bg.setScale(5, BigDecimal.ROUND_HALF_UP);
    return bg.doubleValue();
  }

  public static void main(String args[]) {
    java.awt.EventQueue.invokeLater(new Runnable() {
      public void run() {
        try {
          UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");

        } catch (Exception e) {
        }
        new Main().setVisible(true);
      }
    });
  }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel Pxy;
    private javax.swing.JLabel Qxy;
    private javax.swing.JLabel ascissa;
    private javax.swing.JButton btnadd;
    private javax.swing.JButton btnall;
    private javax.swing.JButton btng;
    private javax.swing.JButton btnrimuovi;
    private javax.swing.JFileChooser chooser;
    private javax.swing.JLabel coef;
    private javax.swing.JLabel covar;
    private javax.swing.JLabel iters;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JList list;
    private javax.swing.JLabel ordinata;
    private javax.swing.JPanel p;
    private javax.swing.JLabel retta;
    private javax.swing.JButton save;
    private javax.swing.JTextField txtdx;
    private javax.swing.JTextField txtdy;
    private javax.swing.JLabel var;
    // End of variables declaration//GEN-END:variables
}
