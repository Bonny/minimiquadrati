
import java.awt.*;
import javax.swing.*;

public class Piano extends JPanel {

    private final double XMAX = 20.0;
    private final double XMIN = -20.0;
    private final double YMAX = 18.7;
    private double YMIN;
    private double UNITA;
    private double m, q;
    private int w, h;
    private boolean flag;
    private Punto v[];

    public Piano(Dimension d) {
        flag = false;
        this.setBackground(Color.white);
        this.setSize(d);
        this.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
    }

    public void set(double _m, double _q, Punto _v[]) {
        flag = true;
        this.m = _m;
        this.q = _q;
        this.v = _v;

    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        //calcola dimensioni area di disegno
        w = getSize().width;
        h = getSize().height;
        //calcola YMAX e unita di misura
        UNITA = (double) (w / (XMAX - XMIN));
        YMIN = YMAX - ((double) h / UNITA);
        //disegna assi

        disegnaAssi(g);
        if (flag) {
            disegnaRetta(g);
            disegnaPunti(g);
            flag = false;
        }

    }

    public Point trasforma(double x, double y) {
        Point schermo = new Point();
        schermo.x = (int) Math.round((x - XMIN) * UNITA);
        schermo.y = (int) Math.round((YMAX - y) * UNITA);
        return schermo;
    }

    public void disegnaAssi(Graphics g) {
        Point p1, p2;
        g.setColor(Color.BLACK);
        g.drawRect(0, 0, w - 1, h - 1);
        p1 = trasforma(XMIN, 0);
        p2 = trasforma(XMAX, 0);
        g.drawLine(p1.x, p1.y, p2.x, p2.y);
        p1 = trasforma(0, YMIN);
        p2 = trasforma(0, YMAX);
        g.drawLine(p1.x, p1.y, p2.x, p2.y);
        g.drawString("0", (getSize().width / 2) - 8, (getSize().height / 2) + 10);
        g.drawString("^", (getSize().width / 2) - 3, 10);
        g.drawString(">", getSize().width - 8, (getSize().height / 2) + 3);
    }

    public void disegnaRetta(Graphics g) {
        Point p1, p2;
        g.setColor(Color.blue);
        p1 = trasforma(-20.0, (m * (-20.0)) + q);
        p2 = trasforma(20.0, (m * 20) + q);
        g.drawLine(p1.x, p1.y, p2.x, p2.y);
    }

    public void disegnaPunti(Graphics g) {
        Point p;
        g.setColor(Color.red);
        for (int i = 0; i < v.length; i++) {
            p = trasforma(v[i].getX(), v[i].getY());
            g.drawOval(p.x, p.y, 5, 5);
        }
    }
}

